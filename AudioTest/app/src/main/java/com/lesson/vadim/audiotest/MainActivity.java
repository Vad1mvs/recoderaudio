package com.lesson.vadim.audiotest;

import android.app.Activity;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MainActivity extends Activity {

    String[] strKHz = {"8.000 KHz", "11.025 KHz", "16.000 KHz", "22.050 KHz", "44.100 KHz"};
    Integer[] intKHz = {8000 ,11025, 16000, 22050, 44100};

    private ArrayAdapter<String> adapter;
    private Spinner spinner;
    private Button btnRecorder, btnPlay, btnBack;
    private TextView tvTimer;
    private static final int MILLIS_PER_SECOND = 1000;
    private static final int SECONDS_TO_COUNTDOWN = 10;
    private CountDownTimer timer;
    private Boolean recording;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnRecorder = (Button)findViewById(R.id.btnRecorder);
        btnPlay = (Button)findViewById(R.id.btnPlay);
        tvTimer = (TextView)findViewById(R.id.tvTimer);
        btnBack = (Button)findViewById(R.id.btnBack);
        spinner = (Spinner)findViewById(R.id.spinner);

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, strKHz);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        //---button recorder listener---
        btnRecorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Thread recordThread = new Thread(new Runnable(){

                    @Override
                    public void run() {
                        recording = true;
                        startRecord();
                    }
                });

                recordThread.start();
                showTimer(SECONDS_TO_COUNTDOWN * MILLIS_PER_SECOND);
                btnRecorder.setEnabled(false);
            }
        });

        //---button play audio listener---
        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playRecord();
                showTimer(SECONDS_TO_COUNTDOWN * MILLIS_PER_SECOND);
                btnPlay.setEnabled(false);
            }
        });

        //---button play back audio---
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playBack();
                showTimer(SECONDS_TO_COUNTDOWN * MILLIS_PER_SECOND);
                btnBack.setEnabled(false);
            }
        });

    }

    private void startRecord(){

        File file = new File(Environment.getExternalStorageDirectory().getAbsoluteFile(), "test.pcm");

        int selectedPos = spinner.getSelectedItemPosition();
        int sampleFreq = intKHz[selectedPos];

        final String spinnerSelectedItem = (String) spinner.getSelectedItem();

        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Toast.makeText(MainActivity.this, "Record " + spinnerSelectedItem, Toast.LENGTH_LONG).show();
            }
        });

        try {
            file.createNewFile();

            OutputStream outputStream = new FileOutputStream(file);
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);
            DataOutputStream dataOutputStream = new DataOutputStream(bufferedOutputStream);

            int minBufferSize = AudioRecord.getMinBufferSize(sampleFreq,
                    AudioFormat.CHANNEL_CONFIGURATION_MONO,
                    AudioFormat.ENCODING_PCM_16BIT);

            short[] audioData = new short[minBufferSize];

            AudioRecord audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC,
                    sampleFreq,
                    AudioFormat.CHANNEL_CONFIGURATION_MONO,
                    AudioFormat.ENCODING_PCM_16BIT,
                    minBufferSize);

            audioRecord.startRecording();

            while(recording){
                int numberOfShort = audioRecord.read(audioData, 0, minBufferSize);
                for(int i = 0; i < numberOfShort; i++){
                    dataOutputStream.writeShort(audioData[i]);
                }
            }

            audioRecord.stop();
            dataOutputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playRecord(){

        File file = new File(Environment.getExternalStorageDirectory(), "test.pcm");
        int shortSizeInBytes = Short.SIZE/Byte.SIZE;

        int bufferSizeInBytes = (int)(file.length()/shortSizeInBytes);
        short[] audioData = new short[bufferSizeInBytes];

        try {
            InputStream inputStream = new FileInputStream(file);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
            DataInputStream dataInputStream = new DataInputStream(bufferedInputStream);

            int i = 0;
            while(dataInputStream.available() > 0){
                audioData[i] = dataInputStream.readShort();
                i++;
            }

            dataInputStream.close();

            int selectedPos = spinner.getSelectedItemPosition();
            int sampleFreq = intKHz[selectedPos];

            final String spinnerSelectedItem = (String) spinner.getSelectedItem();
            Toast.makeText(MainActivity.this, "Play "+ spinnerSelectedItem, Toast.LENGTH_LONG).show();

            AudioTrack audioTrack = new AudioTrack(
                    AudioManager.STREAM_MUSIC,
                    sampleFreq,
                    AudioFormat.CHANNEL_CONFIGURATION_MONO,
                    AudioFormat.ENCODING_PCM_16BIT,
                    bufferSizeInBytes,
                    AudioTrack.MODE_STREAM);

            audioTrack.play();
            audioTrack.write(audioData, 0, bufferSizeInBytes);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void playBack(){
        File file = new File(Environment.getExternalStorageDirectory(), "test.pcm");
        int shortSizeInBytes = Short.SIZE/Byte.SIZE;

        int bufferSizeInBytes = (int)(file.length()/shortSizeInBytes);
        short[] audioData = new short[bufferSizeInBytes];

        try {
            InputStream inputStream = new FileInputStream(file);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
            DataInputStream dataInputStream = new DataInputStream(bufferedInputStream);

            while(dataInputStream.available() > 0){
                for(int i = 0; i <audioData.length; i++)
                audioData[audioData.length - i - 1] = dataInputStream.readShort();
            }

            dataInputStream.close();

            int selectedPos = spinner.getSelectedItemPosition();
            int sampleFreq = intKHz[selectedPos];

            final String spinnerSelectedItem = (String) spinner.getSelectedItem();
            Toast.makeText(MainActivity.this, "Play "+ spinnerSelectedItem, Toast.LENGTH_LONG).show();

            AudioTrack audioTrack = new AudioTrack(
                    AudioManager.STREAM_MUSIC,
                    sampleFreq,
                    AudioFormat.CHANNEL_CONFIGURATION_MONO,
                    AudioFormat.ENCODING_PCM_16BIT,
                    bufferSizeInBytes,
                    AudioTrack.MODE_STREAM);

            audioTrack.play();
            audioTrack.write(audioData, 0, bufferSizeInBytes);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showTimer(int countdownMillis) {
        if(timer != null) { timer.cancel(); }
        timer = new CountDownTimer(countdownMillis, MILLIS_PER_SECOND) {
            @Override
            public void onTick(long millisUntilFinished) {
                tvTimer.setText("Timer: " + millisUntilFinished / MILLIS_PER_SECOND);
            }
            @Override
            public void onFinish() {
                recording = false;
                btnBack.setEnabled(true);
                btnPlay.setEnabled(true);
                btnRecorder.setEnabled(true);
                tvTimer.setText("Recording is completed");
            }
        }.start();
    }

}